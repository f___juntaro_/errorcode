class CreatePostsLabelsCommentsPostsLabels < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      t.text :error
      t.integer :user_id

      t.timestamps
    end
    create_table :labels do |t|
      t.string :tag
      t.integer :post_id

      t.timestamps
    end
    create_table :comments do |t|
      t.text :body
      t.integer :user_id
      t.integer :post_id

      t.timestamps
    end
    create_table :posts_labels do |t|
	  t.integer :post_id
	  t.integer :label_id

      t.timestamps
    end
  end
end
