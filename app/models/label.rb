class Label < ApplicationRecord
  has_many :posts, through: :posts_labels
  has_many :posts_labels
end
