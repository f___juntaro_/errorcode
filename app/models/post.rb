class Post < ApplicationRecord
  belongs_to :user
  has_many :posts_labels
  has_many :labels, through: :posts_labels
  has_many :comments
end
