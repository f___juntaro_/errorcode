class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]

  def index
    @posts = Post.all
    @comment = Comment.new
  end

  def show
    @comments = @post.comments
    @comment = Comment.new
    @comments_count = Comment.where(post_id: @post.id).count
  end

  def new
    @post = Post.new
  end

  def edit
    @comments = @post.comments
    post_id = @post.id
  end

  def create
    @post = Post.new(post_params)
    if @post.save
      redirect_back(fallback_location: root_path)
    else
      redirect_back(fallback_location: root_path)
    end
  end

  def update
    user_id = current_user.id
    if @post.update(post_params)
      redirect_to post_path(@post)
    else
      redirect_to post_path(@post)
    end
  end

  def destroy
    @post.destroy
    redirect_to root_path
  end

  private
    def set_post
      @post = Post.find(params[:id])
    end

    def post_params
      params.require(:post).permit(:error, :user_id, :posts_label_id)
    end
end
